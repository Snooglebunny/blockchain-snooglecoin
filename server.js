/**
* A simple blockchain
* Author: Snooglebunny/Jacky Pham
* Bitbucket: https://bitbucket.org/Snooglebunny/blockchain-snooglecoin
**/

//import the crypto-js library
var SHA256 = require('crypto-js/sha256');

class Block
{
  //all the information that the SHA256() method will scramble.
  constructor(index, timestamp, data, previousHash = '')
  {
    this.index = index;
    this.timestamp = timestamp;
    this.data = data;
    this.previousHash = previousHash;
    this.hash = this.calculateHash();
  }
  //using SHA256 to calculate the hash and then converting the result as a string
  calculateHash()
  {
    return SHA256(this.index + this.previousHash + this.timestamp + JSON.stringify(this.data)).toString();
  }
}//END OF CLASS BLOCK

class Blockchain
{
  constructor()
  {
    this.chain = [this.createGenesisBlock()];
  }
  //creating the first block in the chain. Otherwise known as the Genesis block
  createGenesisBlock()
  {
    return new Block(0, "12/12/2017", "Genisis Block", "0");
  }
  //return the latest block in the chain
  getLatestBlock()
  {
    return this.chain[this.chain.length - 1];
  }
  //adding a new block by getting the latest block hash and pushing the calculated hash onto the stack
  addBlock(newBlock)
  {
    newBlock.previousHash = this.getLatestBlock().hash;
    newBlock.hash = newBlock.calculateHash();
    this.chain.push(newBlock);
  }

  isChainValid()
  {
    for(let i = 1; i < this.chain.length; i++)
    {
      const currentBlock = this.chain[i];
      const previousBlock = this.chain[i-1];
      //check if the current block's hash is the same as the calulated hash for the same block
      if(currentBlock.hash !== currentBlock.calculateHash())
      {
        return false;
      }
      //check if the currentBlock has a broken relation to the hash
      if(currentBlock.previousHash !== previousBlock.hash)
      {
        return false;
      }
    }
    //nothing went wrong therefore return true
    return true;
  }
}//END OF CLASS BLOCKCHAIN

//creating a chain with 2 coins
let snoogleCoin = new Blockchain;
snoogleCoin.addBlock(new Block(1, "12/12/2017", {amount : 4} ));
snoogleCoin.addBlock(new Block(2, "13/12/2017", {amount : 10} ));
//checking chain before tampering
console.log("Is blockchain valid? " + snoogleCoin.isChainValid());
//trying to tamper with the chain
snoogleCoin.chain[1].data = {amount : 1000000};
snoogleCoin.chain[1].hash = snoogleCoin.chain[1].calculateHash();
//checking the chain again.
console.log("Is blockchain valid? " + snoogleCoin.isChainValid());
//remove the below comment if you'd like to to see the data for the chain
console.log(JSON.stringify(snoogleCoin, null, 4));
